# MySQL or MariaDB logical backup

This role installs _mydumper_, a multithreaded logical backup tool. It also installs an open-source wrapper script. 

# Minimum permissions
```sql
CREATE USER logical_backups@localhost IDENTIFIED BY CONCAT('LongerPasswordsAreBetterForSecurity!' + UNIX_TIMESTAMP()); /* please generate a safe password for your backups */
GRANT SELECT, RELOAD, LOCK TABLES, REPLICATION CLIENT, SHOW VIEW, EVENT, TRIGGER on *.* to logical_backups@localhost;
GRANT CREATE on dba.* to logical_backups@localhost;
GRANT INSERT, DELETE, UPDATE, ALTER ON dba.

```

# Example playbook
```yaml
- name: "Configure logical backups"
  serial: 1
  hosts: backup_hosts
  become: true
  roles:
    - role: mysql-mariadb-logical-backup
      mydumper_backup_user: "logical_backups"
      mydumper_backup_password: "LongerPasswordsAreBetterForSecurity!"
  tags: logical-backup-automation
```

# Monitoring

**Automatic monitoring is not enough. Please verify your backups at least twice the retention time.**

Use this query for your monitoring, to monitor the size of the last backup created in the last 36 hours:
`select SUM(size_in_mb) AS backup_size FROM (SELECT 0 AS size_in_mb UNION DISTINCT (SELECT size_in_mb FROM dba.mydumper_history WHERE created_at > NOW() - INTERVAL 36 HOUR AND result='success' AND hostname=(SELECT @@hostname) AND target_host IN ('localhost', '127.0.0.1', (SELECT @@hostname)) order by created_at DESC LIMIT 1)) a;`

After 36 hours of the last backup failing, the size will drop. This query monitors both failed backups and backup size.



# Todo
- CentOS installation
- Improve security (does not have to run as root)
- Restore documentation
- Role documentation
- Better testing

# Pull requests

PR's are very welcome, this role is actively maintained (As of the time of writing, July 2019)!